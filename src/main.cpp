#include <opencv2/opencv.hpp>

using namespace std;

int main( int argc, char** argv )
{
    // Read image
    cv::Mat img = cv::imread("../res/paris.jpg");

    cout << "Input image size: " << img.size() << endl;
    cout << "Input image number of channels: " << img.channels() << endl;

    // Convert it to black and white (3-channel BGR to 1-channel grayscale)
    cv::Mat workingImg;
    cv::cvtColor(img, workingImg, cv::COLOR_BGR2GRAY);

    cout << "Channels after BGR2GRAY conversion: " << workingImg.channels() << endl;

    // Convert it back to 3-channel
    cv::cvtColor(workingImg, workingImg, cv::COLOR_GRAY2BGR);

    cout << "Channels after GRAY2BGR conversion: " << workingImg.channels() << endl;

    // Set JPEG parameters to convert the frame to a byte array later
    std::vector<int> vParams(2);
    vParams[0] = cv::IMWRITE_JPEG_QUALITY;
    vParams[1] = 70;

    // Convert this image to a JPEG byte array

    std::cout << "JPEG conversion..." << endl;

    std::vector<unsigned char> vBytes;
    cv::imencode(".jpg", workingImg, vBytes, vParams);

    // Display the size of the JPEG byte array
    std::cout << "JPEG size: " << vBytes.size() << " bytes" << endl;
}